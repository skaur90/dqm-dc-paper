# dqm-dc-paper

Follow the instruction below, to copy folder in your local workplace:

1. git clone git@gitlab.com:skaur90/dqm-dc-paper.git
2. cd dqm-dc-paper/DQM-DC_Paper
3. In this directroy there are many files with extension .tex, corresponding to different sections of the paper
4. Please don't touch this file: There is one main tex file named **"DQM_DC_Paper.tex"** , which includes input for different section and all other format seeting 
5. How to edit differrent sections:e.g you want to make contibution in the introduction section, edit this file **"Introduction.tex"**
6. To see whether your changes appeared or not in original file, run follwoing command: **pdflatex DQM_DC_Paper.tex**
7. Once you have desired changes in the file, use following commands to updated and commit your file on gitlab
8. git init
9. git remote add origin git@gitlab.com:skaur90/dqm-dc-paper.git
10. git add <file name>
11. git commit -m "messsage"
12. git pull origin master
13. git push origin master
    

